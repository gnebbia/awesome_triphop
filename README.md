## Bands

* massive attack
* kidneythieves
* MrMoods
* Laika
* Red Snapper
* black Era
* bowery electric
* moth equals
* snake river conspiracy
* Boztown
* the chemical brothers
* kryptic minds
* 21 Hertz
* Blicq
* Twin Muses
* Blue Foundation
* Cinephile
* Elenika
* Porcelaintoy
* Puracane
* Sound me
* zelmershead
* emiliana torrini
* amon tobin
* Doctor Bionic
* sneaker pimps
* 8mm
* tricky
* 99 posse
* lamb
* hayling
* aphex twin
* the internet
* portishead
* morcheeba
* UNKLE
* pendulum
* DJ Krush 
* groove armada


## Songs

* Tracing Arcs - Closer To You
* Ghoul - Fiend (chilled trip hop)
* Sneaker Pimps - SP4 (Full Album) Remastered
* DJ Shadow - Midnight in a Perfect World
* Unkle - Bloodstain
* max romeo - chase the devil
* fckahuna  - hayling
* 99 posse  - corto circuito
* DJ Krush  - kemuri
* emiliana torrini - gun
* Bloodsport - Sneaker Pimps
* Low Place Like Home - Sneaker Pimps
* Sneaker Pimps - In The Blue (Single)
* Puracane - Anyone Else
* DJ Shadow - My Lonely Room
* nohue - home
* NAMO - Chiba City Funk
* The Hundred In The Hands - Keep It Low
* Moth Equals - Woebegone
* Laika - Prairie Dog
* Red Snapper - 3 strikes and you're Out

## Hack

search for cyberpunk mix
afteglow
human action network


also give a check to:
[programmer's music](https://www.programmersmusic.com/)

## Bandcamp

https://mrmoods.bandcamp.com/album/the-trip-hop-sessions (twin muses)
